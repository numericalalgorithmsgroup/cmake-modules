# syntax=docker/dockerfile:1

FROM debian:latest
WORKDIR /home

# Identify the maintainer of an image
LABEL maintainer="simon.maertens@nag.co.uk"

# Update the image to the latest packages
RUN apt-get update && apt-get upgrade -y

# Install dependencies via apt
RUN apt-get install build-essential -y
RUN apt-get install git -y
RUN apt-get install gfortran -y
RUN apt-get install python3-full -y
RUN apt-get install python3-pip -y
RUN apt-get install clang-11 -y
RUN apt-get install iwyu -y

# Init virtual python environment
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN python3 -m pip install --upgrade pip

# Install newer versions of dependencies via pip in virtual environment
RUN python3 -m pip install numpy
RUN python3 -m pip install sympy
RUN python3 -m pip install scipy
RUN python3 -m pip install cmake
RUN python3 -m pip install ninja
RUN python3 -m pip install gcovr
RUN python3 -m pip install lcov
RUN python3 -m pip install fastcov
RUN python3 -m pip install clang-format
RUN python3 -m pip install clang-tidy
RUN python3 -m pip install cmakelang
RUN python3 -m pip install PyYAML
